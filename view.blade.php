<!-- Start GoodbyeWorld -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A brief desc -->
@endif
<div class="goodbye-world"  is="fir-goodbye-world">
  <script type="application/json">
      {
          "options": @json($jsonData)
      }
  </script>
  <div class="goodbye-world__wrap">
      Pinecone: GoodbyeWorld / GoodbyeWorld
  </div>
</div>
<!-- End GoodbyeWorld -->