class GoodbyeWorld extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.$ = $(this)
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initGoodbyeWorld()
    }

    initGoodbyeWorld () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: GoodbyeWorld")
    }

}

window.customElements.define('fir-goodbye-world', GoodbyeWorld, { extends: 'div' })
